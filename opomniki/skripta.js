window.addEventListener('load', function() {
	//stran nalozena
	
	
	// vpis uporabnika
	var vpisUporabnika = function(){
		var trenutniUporabnik = document.querySelector("#uporabnisko_ime").value;
		document.querySelector("#uporabnik").innerHTML = trenutniUporabnik;
		document.querySelector(".pokrivalo").style.visibility = "hidden";
	};
	document.querySelector("#prijavniGumb").addEventListener('click', vpisUporabnika);
	
	
	// dodaj opomnik
	var dodajOpomnik = function(){
		var trenutniOpomnik = document.querySelector("#naziv_opomnika").value;
		var trenutniCas = document.querySelector("#cas_opomnika").value;
		document.querySelector("#naziv_opomnika").value = "";
		document.querySelector("#cas_opomnika").value = "";
		document.querySelector("#opomniki").innerHTML += " \
			<div class='opomnik senca rob'> \
    			<div class='naziv_opomnika'> " + trenutniOpomnik + " </div> \
    			<div class='cas_opomnika'> Opomnik čez <span>" + trenutniCas + "</span> sekund. </div> \
    		</div>";
	}
	document.querySelector("#dodajGumb").addEventListener('click', dodajOpomnik);

	//Posodobi opomnike
	var posodobiOpomnike = function() {
		var opomniki = document.querySelectorAll(".opomnik");

		for (var i = 0; i < opomniki.length; i++) {
			var opomnik = opomniki[i];
			var casovnik = opomnik.querySelector("span");
			var cas = parseInt(casovnik.innerHTML);
			var naziv = opomnik.querySelector(".naziv_opomnika").innerHTML;

			//TODO:
			// - če je čas enak 0, izpiši opozorilo "Opomnik!\n\nZadolžitev NAZIV_OPOMNIK je potekla!"
			// - sicer zmanjšaj čas za 1 in nastavi novo vrednost v časovniku
			
			if (cas == 0){
				alert("Opomnik!\n\nZadolžitev " + naziv + " je potekla!");
				document.querySelector("#opomniki").removeChild(opomnik);
			}
			else {
				cas -= 1;
				casovnik.innerHTML = cas;
			}
		}
	}
	setInterval(posodobiOpomnike, 1000);

});
